#!/usr/bin/perl
#
# Merge the source files from no.speling.org with the spell-norwegian
# data file norsk.words to produce a updated norsk.words with the new
# words.

use warnings;
use strict;

use Data::Dumper;

my $words= $ARGV[0] || "norsk.words";

my $nbsource= $ARGV[1] || "source-nb";
my $nbgood = $ARGV[2] || "speling-good.nb";

my $nnsource= $ARGV[3] || "source-nn";
my $nngood = $ARGV[4] || "speling-good.nn";

my $wordboundary = "="; # Marks word boundary
my $trimarker = '"'; # Marks contracted tripple consonants

my %word;

my $debug = 0;

# Make this false to add new words as well as update the old ones.
my $updateonly = 0;

# Set this to true if you want to load word boundaries from speling.org
# Only boundary info provided by at least two entries (people) are
# considered.
my $load_word_boundaries = 1;

my @headers = ();
load_norsk_words($words);

# read source-nb and source-nn
load_source($nbgood, $nbsource, "B", "0");
load_source($nngood, $nnsource, "*", undef);

#print Dumper(%word) if $debug

# print all words
print @headers;
for my $w (sort { $word{$a}{entry} cmp $word{$b}{entry} } keys %word) {
    my $entry = $word{$w}{entry};

    print $entry;
    my @restkeys = ();

    for my $key (@{$word{$w}{keys}}) {
        if ("*" eq $key) {
            print " ", $key;
        } else {
            push @restkeys, $key;
        }
    }
    print " ", $word{$w}{freq} if (exists $word{$w}{freq});
    print " ", join(" ", reduce_word_keys(@restkeys)) if @restkeys;
    print "\n";
}

sub maptripple {
    my $entry = shift;
    # Try to handle triple consonants
    $entry =~ s/dd${wordboundary}d/\"dd/g;
    $entry =~ s/kk${wordboundary}k/\"kk/g;
    $entry =~ s/ll${wordboundary}l/\"ll/g;
    $entry =~ s/nn${wordboundary}n/\"nn/g;
    $entry =~ s/pp${wordboundary}p/\"pp/g;
    $entry =~ s/ss${wordboundary}s/\"ss/g;
    $entry =~ s/tt${wordboundary}t/\"tt/g;

    return $entry;
}

sub load_norsk_words {
    my ($words) = @_;

    # read norsk.words
    open (F, "<", $words) || die;
    while (<F>) {
        if (/^\#/) {
            push @headers, $_;
            next;
        }
        chomp;
        my ($entry, $keystr) = m/^(\S+)\s+(.+)$/;
        my $w = $entry;
        $w =~ s/\"//g;

        # Expand tripple consonants to match the no.speling.org
        # entries.
        $entry =~ s/\"dd/dd${wordboundary}d/g;
        $entry =~ s/\"kk/kk${wordboundary}k/g;
        $entry =~ s/\"ll/ll${wordboundary}l/g;
        $entry =~ s/\"nn/nn${wordboundary}n/g;
        $entry =~ s/\"pp/pp${wordboundary}p/g;
        $entry =~ s/\"ss/ss${wordboundary}s/g;
        $entry =~ s/\"tt/tt${wordboundary}t/g;

        $w =~ s/$wordboundary//g;
        my @keys = split(/ +/, $keystr);
        if (exists $word{$w}) {
            print STDERR "info: Duplicate entry $w -> $entry\n" if $debug;
        } else {
            $word{$w}{entry} = maptripple($entry);
            add_word_keys($w, @keys);
        }
    }
    close(F);
}

sub is_word_ok {
    my ($word) = @_;

    # Skip words with the word boundary character, as we can't handle
    # them at the moment [pere 2007-11-05]
    return 0 if $word =~ m/$wordboundary/;

    # Refuse entries with word boundary markers in the words
    # themselves.  The entries are bogus, as these markers are illegal
    # there.
    return 0 if $word =~ m/=/;
    return 0 if $word =~ m/~/;

    # Skip words with space in them, as the spell checkers can't
    # check such words.
    return 0 if $word =~ m/ /;

    # Skip words starting or ending with dash.  Cruft from ordbanken
    return 0 if $word =~ m/^-/ || $word =~ m/-$/;

    # Skip words with period in them, as the spell checkers can't
    # check such words very well.
    return 0 if $word =~ m/\./;

    # Skip words with the trimarker in them, as they should never have
    # been submitted to speling.org.
    return 0 if $word =~ m/$trimarker/;

    # Skip words with single quote in them.  It confuses the spell
    # checkers
    return 0 if $word =~ m/\'/;

    # Skip words with slash in them.  It confuses the spell
    # checkers
    return 0 if m%/%;

    # Until we figure out all the strange characters in the words from
    # no.speling.org and ordbanken, only accept known
    # characters. [pere 2007-11-05]
    return unless $word =~ m/^[-a-c�d-e����f-o���p-u�v-z�����A-C�D-E����F-O���P-U�V-Z�����]+$/;

    # Skip short words 1-2 characters
    return 0 if length($_) < 3;

    return 1;
}

# Load the word boundary markers from the source file, and select the
# entry with the most markers
sub load_source_word_boundaries {
    my ($source) = @_;
    my $w;
    my $status = "-";
    my %boundaries = ();
    open(F, "<", $source) || die;
    while (<F>) {
        chomp;
        if (m/^WORD: (.+)$/) {
            if ($w && $w ne $1 && exists $word{$w}) {
                my $entry = $w;
                for my $key (sort { $boundaries{$a} <=> $boundaries{$b} }
                             keys %boundaries) {
                    # Sorting it make sure the one with most entries
                    # is selected, as long as there is more than one
                    # entry.
                    $entry = $key if (1 < $boundaries{$key});
                }
                if (exists $word{$w}{entry}) {
                    if (length($word{$w}{entry}) <= length($entry)) {
                        $word{$w}{entry} = maptripple($entry);
                    }
                } else {
                    $word{$w}{entry} = maptripple($entry);
                }
                %boundaries = ();
            }
            $w = $1;
        }

        $status = $1 if m/^STATUS: (.+)$/;
        if ("+" eq $status && m/^COMPOSITE-WORD: (.+)$/) {
            # Only process good words
            if (exists $word{$w}) {
                my $new = $1;

                # Translate word boundary markers to the one used in
                # spell-norwegian.
                $new =~ s/=/$wordboundary/g;
                $new =~ s/~/$wordboundary/g;

                my $test = $new;
                $test =~ s/$wordboundary//g;
                unless (is_word_ok($test) || $test ne $w) {
                    print STDERR "warning: Rejecting composition '$new' -> '$test' (source $source)\n";
                } else {
                    # Try to handle triple consonants
                    $new =~ s/dd${wordboundary}d/\"dd/g;
                    $new =~ s/kk${wordboundary}k/\"kk/g;
                    $new =~ s/ll${wordboundary}l/\"ll/g;
                    $new =~ s/nn${wordboundary}n/\"nn/g;
                    $new =~ s/pp${wordboundary}p/\"pp/g;
                    $new =~ s/ss${wordboundary}s/\"ss/g;
                    $new =~ s/tt${wordboundary}t/\"tt/g;

                    $boundaries{$new}++;
                }
            }
        }
    }
    if ($w && exists $word{$w}) {
        my $entry = $w;
        for my $key (sort { $boundaries{$a} <=> $boundaries{$b} }
                     keys %boundaries) {
            # Sorting it make sure the one with most entries
            # is selected, as long as there is more than one
            # entry.
            $entry = $key if (1 < $boundaries{$key});
        }
        if (exists $word{$w}{entry}) {
            if (length($word{$w}{entry}) <= length($entry)) {
                $word{$w}{entry} = maptripple($entry);
            }
        } else {
            $word{$w}{entry} = maptripple($entry);
        }
    }
    close(F);
}

sub load_source {
    my ($good, $source, $key, $freq) = @_;

    # First load all the good words
    open(F, "<", $good) || die;
    while (<F>) {
        chomp;
        unless (is_word_ok($_)) {
            print STDERR "warning: Rejecting word '$_' (source $good)\n"
                if (!exists $word{$_});
            next;
        }
        if (exists $word{$_}) {
            print STDERR "info: Duplicate entry $_\n" if $debug;
            add_word_keys($_, $key);
        } elsif (!$updateonly) {
            $word{$_}{entry} = maptripple($_);
            $word{$_}{freq} = $freq if defined $freq;
            add_word_keys($_, $key);
        }
    }
    close(F);

    load_source_word_boundaries($source) if ($load_word_boundaries);
}

sub add_word_keys {
    my ($word, @keys) = @_;
    my %newkeys;
    my @oldkeys = @{$word{$word}{keys}} if (exists $word{$word}{keys});
    for my $key (@keys, @oldkeys) {
        if ($key =~ m/^\d+$/) {
            $word{$word}{freq} = $key;
        } else {
            $newkeys{$key} = 1;
        }
    }
    $word{$word}{keys} = [keys %newkeys];
}

# Reduce the list of keys for bokm�l from several to one, in the given
# priority
sub reduce_word_keys {
    my @keys = @_;
    my @prioritylist = qw(B A N M K D O C S);
    my $priority = scalar @prioritylist;
    for my $p (@prioritylist) {
        for my $key (@keys) {
            return $p if ($p eq $key);
        }
    }
}
