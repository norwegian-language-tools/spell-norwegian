#!/bin/sh
# Run like this:
# ./check_words.sh missingwordsfile
#This probably only works on linux
# Checks to find missing words in the wordlist
# Takes file with missing words
# checks to see if word is present in word list for bokm�l or nynorsk
# outputs list of missing words.
# Input file has to have on word on each line, it will only take the first word on the line.
# Outputs on screen words that are not found
# Do first:
# RUn make -f Makefile.new aspell-dist this first
#
#Should print whole wordlist line to understand why words are missing.
WORDLISTNB="../norsk.words"
WORDLISTNN="../words.nynorsk"
FORKORT="../forkortelser.txt"
CHECKWORDS=$1

WORDS=`cat $CHECKWORDS | grep -v ^# | cut -d ' ' -f 1`
echo "searching bokm�lslist"
for i in $WORDS; do
	RESULT=`grep -i ^$i[[:space:]] $WORDLISTNB`
	if [ -z "$RESULT" ]; then
		echo "Missing word " $i
	else 
	echo "word present  " $RESULT
	fi
done;

echo "checking nynorsk"
for i in $WORDS; do
	RESULT=`grep -i ^$i$ $WORDLISTNN`
	 if [ -z "$RESULT" ]; then
                echo "Missing word " $i
        else
echo "word present  " $RESULT
	fi
done;
