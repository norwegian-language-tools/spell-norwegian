# Makefile for the Norwegian dictionary for Ispell v. 2.0
# Copyright (C) 2000, Rune Kleveland, 2005-2006 the spell-norwegian
# maintainers
#
# Maintainer: The spell-norwegian maintainer group
# Email : i18n-no@lister.ping.uio.no
# License : GNU General Public License v2 or later

VERSION         = 2.2
PACKAGE         = spell-norwegian

prefix          = /usr
libdir          = $(prefix)/lib
sharedir        = $(prefix)/share
bindir          = $(prefix)/bin
docdir          = $(sharedir)/doc/$(PACKAGE)
wordsdir        = $(sharedir)/dict
ispelldir       = $(libdir)/ispell
myspelldir      = $(sharedir)/myspell/dicts
hyphendir       = $(myspelldir)
thesdir         = $(myspelldir)
aspelldir       = $(libdir)/aspell
aspellsharedir  = $(sharedir)/aspell

# Make sure we work with a known locale setting.
export LC_ALL=C

SHELL = /bin/sh
MAKE = make
INSTALL      = install -m 755
INSTALL_DATA = install -m 644

PATHADDER	=	../..
BUILDHASH	=	buildhash
ASPELL          =       aspell
ISPELL          =       ispell
ISPELLAFF2MYSPELL =     ispellaff2myspell
MUNCHLIST       =       munchlist
AWK		=	awk
SED		=	ssed
PERL            =       perl
GREP            =       grep -a
EGREP           =       egrep

# List of characters used in the thesaurus files
theschars = abcdefghijklmnopqrstuvwxyz���ABCDEFGHIJKLMNOPQRSTUVWXYZ���-()\#

ECHO            =       echo

export PATH:=$(PATHADDER):$(PATH)

DOC = \
	COPYING \
	NEWS \
	README \
	README.myspell \
	thesaurus-README.txt

# Files to include in tarball
EXTRA_DIST = \
	$(DOC) \
	ispell-3.1.20.no.patch \
	ispell-3.2.06.no.patch \
	aspell-nb.info.in \
	aspell-nn.info.in \
	aspell-proc.pl \
	aspell-phonet.dat \
	bokmaal \
	forkort-nb.txt \
	forkort-nn.txt \
	inorsk-compwordsmaybe \
	inorsk-hyphenmaybe \
	Makefile \
	Makefile.new \
	nohyphbc.tex \
	nohyphb.tex \
	nb.aff.in \
	nn.aff.in \
	norsk.cfg \
	norsk.words \
	missing.nb \
	rejected-words.nb \
	missing.nn \
	words_clean.nynorsk \
	thesaurus-nb.txt \
	thesaurus-nn.txt \
	nb_NO.myheader \
	nn_NO.myheader \
	iaff2myaff.pl


# Directories to include in tarball
SUBDIRS = \
	missing \
	scripts \
	patterns \
	ooo-hyph

srcdir      := .
distdir     := $(PACKAGE)-$(VERSION)
top_distdir := $(distdir)
DISTFILES    = $(EXTRA_DIST)
TAR          = tar
GZIP_ENV     = --best

# The following variables make it easy to adapt this Makefile to
# numerous languages.
#
LANGUAGE	=	norsk

#
#	Set this to "-vx" in the make command line if you need to
#	debug the complex shell commands.
#
SHELLDEBUG = +vx

# Some technical variables for managing hyphen points and the header

# CATNOHEADER=$(SED) -e '/^\#/ D' -e 's/[	]*\#.*//' ${LANGUAGE}.words
CATNOHEADER=$(GREP) -v '^\#' ${LANGUAGE}.words
ALPHASUBST=tr '=' '�'
ALPHASUBSTSED=s/=/�/g
STREKSUBST=tr '�' '='
STREKSUBSTSED=s/�/=/g
STREKREM=tr -d '��'
STREKREMSED=s/[��]//g

# What characters and flags do we use for Norwegian?

# Not including dash (-) in LCH and UCH, as - has special meaning in
# regex and need to be listed first and only one.  Adding it to CH
# instead.

LCH=\"a-z���������������
UCH=A-Z���������������
CH=-${LCH}${UCH}
PRE=a-s
SUFFNORM=][t-zA-Z^
SUFFCOMP=\\\\\\\`_
SUFF=${SUFFNORM}${SUFFCOMP}
#SUFF=][t-zA-Z\\\\\\\`^_


# The awk scripts below tells which words from in each category that
# should be in the dictionary.  The line

# /^[${LCH}=]{4}\[${SUFF}]/   {if ($$2>4) {print $$1,$$2}}

# says that words with length 4 containing only lowercase letters with
# frequency greater then 5 should be included.  Edit the scripts as
# you like, but please don't make syntactic errors.  Awk forgives
# nothing.  Remember that it is more likely that a mistyping turns out
# to be a legal word if the word is short.  `re' is legal!

# The CHOOSEFLAG script sets the limit for flag inclusion.  Example:
# adgangs-tegn is a common word, but the form adgangstegnenes scores 0
# on frequency.  It will be excluded by the script below if you don't
# change it.

# The CHOOSEROOT script selects the root words to be included after
# the uncommonly used flags have been deleted.  The key used is the
# frequency category of of the union of all words this root and its
# flags generates.

# The reason for the two pass system is that the space required by a
# root word is much bigger than what is required by just a flag.

# You are of course free to change the selection system.

# The B file contains all kinds of words.


CATHEGORIES=B A N M K D O C

# Configuration for the words file.

# The words file is a plain text file containing words in alphabetical
# order.  It is used by ispell via the look/grep programs to display
# words starting with a specific string or matching a specific
# pattern.  It is also useful if one want to make a dictionary for
# some stupid spell-checker in a word processor.

# Lets make this simple.

WORDSFILTER='{if ($$2>=0) {print $$1}}'

# Configuration for building ispell dictionaries.

# The frequency category works best for B, A and N categories.

# Very young people and people which don't speak Norwegian natively
# will probably be much more happy with a smaller dictionary than the
# complete one.  A smaller dictionary should also be considered if the
# machine is low on memory.  Below is a quite advanced system for
# building such dictionaries.

# It is possible to remove all words that is accepted by ispell in
# controlled compoundwords mode with frequency indicator less than
# COMPOUNDLIMIT.  Thus if `naturvern' and `direktorat' are words which
# is marked as allowed in compounds, it might not be nessesary to
# include `naturverndirektorat' in the dictionary.

# However, if one misspells `naturverndirektorat', ispell will not be
# able to make a suggestion for this word.  And one must use the
# controlled compoundwords mode to accept this word, and that is not
# as secure as the -B mode.

# In sum; It is nice if ispell can make a suggestion words like
# `angrefrisperiode', but it consumes space and memory.

COMPOUNDLIMIT=0

# There is a system for selecting words to include in the Ispell
# dictionary.  Unfortunately it is rather complex and not too easy to
# use, but this was what I came up with, so you must use it or invent
# your own.  The good thing is that one can select both flags and
# roots, depending on how the root word looks line and the frequency
# og all forms coming from the flag or the root.

# Somewhere in the long pipe making the input file for buildhash, the
# data looks like
#
# gutte=dr�m/ 17
# gutte=dr�m/A 18
# gutte=dr�m/E 14
# gutte=dr�m/G 7
#
# thus the frequenzy indicator for each flag is availiable.  Awk is
# used to pick the flags we want, and the variable holding the program
# is CHOOSEFLAG[CATHEGORY]. Don't throw away the root, since that
# messes things up badly.
#
# Later in the pipe the data looks like
#
# gutte=dr�m/17A18E14G7 19
#
# The second field (19) is the frequenzy indicator for all words
# coming from the root gutt.  So here we can throw away a root with
# all its derivied forms if we like.

DEFAULTROOTFILTER='{print $$1,$$2}' # This selects all words in a file

# Don't include all rare words allowed in compounds.

define DEFAULTFLAGFILTER
'!/[${SUFFCOMP}]/      {print $$1,$$2} \
/\/[${SUFFCOMP}]/      {if ($$2>6) {print $$1,$$2}}'
endef

# Select all words by default.  Then override with more elaborate
# rules if desired.

CHOOSEFLAGA=${DEFAULTFLAGFILTER} # `newspaper' words, but very useful.
CHOOSEFLAGB=${DEFAULTFLAGFILTER} # Normal words
CHOOSEFLAGC=${DEFAULTFLAGFILTER} # Sammendragning
CHOOSEFLAGD=${DEFAULTFLAGFILTER} # Words from Dagbladet
CHOOSEFLAGK=${DEFAULTFLAGFILTER} # Conservative writing
CHOOSEFLAGM=${DEFAULTFLAGFILTER} # Words from mathematics
CHOOSEFLAGN=${DEFAULTFLAGFILTER} # Words from NOU
CHOOSEFLAGO=${DEFAULTFLAGFILTER} # Words from technical oil business
CHOOSEFLAGS=${DEFAULTFLAGFILTER} # Samnorsk, radical forms

CHOOSEROOTA=${DEFAULTROOTFILTER} # `newspaper' words, but very useful.
CHOOSEROOTB=${DEFAULTROOTFILTER} # Normal words
CHOOSEROOTC=${DEFAULTROOTFILTER} # Sammendragning
CHOOSEROOTD=${DEFAULTROOTFILTER} # Words from Dagbladet
CHOOSEROOTK=${DEFAULTROOTFILTER} # Conservative writing
CHOOSEROOTM=${DEFAULTROOTFILTER} # Words from mathematics
CHOOSEROOTN=${DEFAULTROOTFILTER} # Words from NOU
CHOOSEROOTO=${DEFAULTROOTFILTER} # Words from technical oil business
CHOOSEROOTS=${DEFAULTROOTFILTER} # Samnorsk, radical forms

# Here is an example of an awk script that excludes some short
# uncommon words, and some long very uncommon words.  It is most
# likely that you want to exclude short uncommon words, since those
# are most likely to be typed by mistake.  Exclude long words to save
# memory.  Also see the COMPOUNDLIMIT variable above if you want to
# make a resource-friendly dictionary.

# define CHOOSEFLAGB
# '/\/[ ${PRE}]/                                  {print $$1,$$2}  \
# /^[${LCH}=]{1,2}\/[${SUFF}]/        {if ($$2>6) {print $$1,$$2}} \
# /^[${LCH}=]{3}\/[${SUFF}]/          {if ($$2>5) {print $$1,$$2}} \
# /^[${LCH}=]{4}\/[${SUFF}]/          {if ($$2>3) {print $$1,$$2}} \
# /^[${LCH}=]{5,7}\/[${SUFF}]/        {if ($$2>1) {print $$1,$$2}} \
# /^[${LCH}=]{8,}\/[${SUFF}]/         {if ($$2>=0) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{1,2}\/[${SUFF}]/ {if ($$2>4) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{3}\/[${SUFF}]/   {if ($$2>3) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{4}\/[${SUFF}]/   {if ($$2>2) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{5,7}\/[${SUFF}]/ {if ($$2>1) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{8,}\/[${SUFF}]/  {if ($$2>=0) {print $$1,$$2}}'
# endef

# define CHOOSEROOTB
# '/^[${LCH}=]{1,2}\//       {if ($$2>8) {print $$1,$$2}} \
# /^[${LCH}=]{3}\//          {if ($$2>6) {print $$1,$$2}} \
# /^[${LCH}=]{4}\//          {if ($$2>5) {print $$1,$$2}} \
# /^[${LCH}=]{5,7}\//        {if ($$2>2) {print $$1,$$2}} \
# /^[${LCH}=]{8,}\//         {if ($$2>1) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{1,2}\// {if ($$2>8) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{3}\//   {if ($$2>6) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{4}\//   {if ($$2>3) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{5,7}\// {if ($$2>2) {print $$1,$$2}} \
# /^[${UCH}][${CH}=]{8,}\//  {if ($$2>1) {print $$1,$$2}}'
# endef

# Try to make it easier to change the OOo2 build variables
# and to make them consistent
TH_NB1 = th_nb_NO
TH_NN1 = th_nn_NO
TH_NB2 = th_nb_NO_v2
TH_NN2 = th_nn_NO_v2
TH_NB1_DICT = 'THES nb NO' $(TH_NB1)
TH_NN1_DICT = 'THES nn NO' $(TH_NN1)
TH_NB2_DICT = 'THES nb NO' $(TH_NB2)
TH_NN2_DICT = 'THES nn NO' $(TH_NN2)



all: words ispell myspell aspell
ispell: nb.hash nn.hash nb.aff nn.aff
words: words.nb words.nn

install: install-ispell install-words install-aspell install-myspell install-scripts install-doc

install-doc: $(DOC)
	$(INSTALL) -d $(DESTDIR)$(docdir)
	for d in $(DOC) ; do \
		$(INSTALL_DATA) $$d $(DESTDIR)$(docdir)/$$d ; \
	done

install-ispell: install-ispell-nb install-ispell-nn
install-ispell-nb: nb.hash nb.aff
	$(INSTALL) -d $(DESTDIR)$(ispelldir)
	$(INSTALL_DATA) nb.hash $(DESTDIR)$(ispelldir)/nb.hash
	$(INSTALL_DATA) nb.aff $(DESTDIR)$(ispelldir)/nb.aff
	if true ; then \
	    ln -sf $(ispelldir)/nb.hash $(DESTDIR)$(ispelldir)/bokmaal.hash ; \
	    ln -sf $(ispelldir)/nb.aff $(DESTDIR)$(ispelldir)/bokmaal.aff ; \
	fi
	if true ; then \
	    ln -sf $(ispelldir)/nb.hash $(DESTDIR)$(ispelldir)/norsk.hash ; \
	    ln -sf $(ispelldir)/nb.aff $(DESTDIR)$(ispelldir)/norsk.aff ; \
	fi

install-ispell-nn: nn.hash nn.aff
	$(INSTALL) -d $(DESTDIR)$(ispelldir)
	$(INSTALL_DATA) nn.hash $(DESTDIR)$(ispelldir)/nn.hash
	$(INSTALL_DATA) nn.aff $(DESTDIR)$(ispelldir)/nn.aff
	if true ; then \
	    ln -sf $(ispelldir)/nn.hash $(DESTDIR)$(ispelldir)/nynorsk.hash ; \
	    ln -sf $(ispelldir)/nn.aff $(DESTDIR)$(ispelldir)/nynorsk.aff ; \
	fi

install-words: install-words-nb install-words-nn
install-words-nb: words.nb
	$(INSTALL) -d $(DESTDIR)$(wordsdir)
	$(INSTALL_DATA) words.nb $(DESTDIR)$(wordsdir)/bokmaal
	ln -sf bokmaal $(DESTDIR)$(wordsdir)/bokm�l
install-words-nn: words.nn
	$(INSTALL) -d $(DESTDIR)$(wordsdir)
	$(INSTALL_DATA) words.nn $(DESTDIR)$(wordsdir)/nynorsk

install-scripts:  inorsk-compwordsmaybe inorsk-hyphenmaybe
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL) inorsk-compwordsmaybe inorsk-hyphenmaybe $(DESTDIR)$(bindir)

nb.hash nn.hash: %.hash: %.mch %.aff
	rm -f $@
	${BUILDHASH} $< $(subst .hash,.aff,$@) $@

nb.aff nn.aff: %.aff: %.aff.in
	$(SED) -e 's/stringchar *� *�//' -e '$(STREKREMSED)' $< > $@.new && \
	  mv $@.new $@

nb.aff.munch nn.aff.munch: %.aff.munch: %.aff.in
	( \
	  $(SED) -e 's/\(.*> *[-,${UCH}]*\)    \( *#~.*$$\)/\1XXXX\2 *HACK*/' \
	    -e 's/-ZYZYZY,-\( *#-.*$$\)/-ZYZYZY,ZYZYZY\1/' \
	    -e 's/\(^flag  *\)~\(..\?:\)/\1\2/'  \
	    -e 's/^\(compoundwords\) controlled z/\1 off/' $< ; \
	  $(ECHO) ; \
	  $(ECHO) ; \
	  $(ECHO) 'flag z: # Brukes for � bevare z-flagg gjennom munchlist' ;\
	  $(ECHO) '    .              >       YYYY            # *HACK*' \
	) > $@.new && mv $@.new $@

nb.munch.hash nn.munch.hash: %.munch.hash: %.aff.munch
	echo 'QQQQQQQQ' > FAKEDICT
	${BUILDHASH} -s FAKEDICT $< $@
	rm -f FAKEDICT FAKEDICT.cnt FAKEDICT.stat

nb.aff.null nn.aff.null: %.aff.null: %.aff.in
	$(SED) -e '/^prefixes.*/,//d' $< > $@.new
	$(ECHO) 'suffixes' >> $@
	$(ECHO) 'flag *z:' >> $@
	$(ECHO) 'Y Y Y Y Y   >   YYYYYY' >> $@.new
	mv $@.new $@

# The following ugly code munches a part of the base file, keeping the
# indications of the frequency of the words.  It also removes some
# redundant flags that munchlist does not find.  That part could be
# improved.


munched.%: ${LANGUAGE}.words nb.aff.munch nb.munch.hash
# The first pipe produces a list of all words in the % category, with
# each root word followed by one line for each flag containing the
# root word and the flag.  The prefix flags are treated as part of the
# root word, except that there is one line containing just the root
# word (the last bug I catched...) The hyphen character is ignored
# when the list is sorted.  Some redundant flags are also removed.
# Isn't it amazing how much you can do with sed?

# If we try to munch the whole B dictionary in one run, ispell will
# probably dump core.  This happens when one gets `hash overflows'.
# Check the log, and change the splitting (^[${UCH}]) if nessesary.
# Nasty bug, and very silent.
	${CATNOHEADER} \
	  | $(GREP) -e '$(subst munched.,,$@)$$' \
	  | $(SED) -e 's/ .*//' -e '$(ALPHASUBSTSED)' -e 's/ \*//' \
	  | $(GREP) '^[${UCH}]' \
	  | $(MUNCHLIST) -v -l nb.aff.munch \
	  > munch1.tmp
	${CATNOHEADER} \
	  | $(GREP) -e '$(subst munched.,,$@)$$' \
	  | $(SED) -e 's/ .*//' -e '$(ALPHASUBSTSED)' -e 's/ \*//' \
	  | $(GREP) -v '^[${UCH}]' \
	  | $(MUNCHLIST) -v -l nb.aff.munch \
	  >> munch1.tmp
	cat munch1.tmp \
	  | $(SED) -e 's/\(zyzyzy\|ZYZYZY\)/\1\/\` /' \
	  | $(SED) -e 's/^\(.*\)$$/----\1\*\1/' \
	  | tr '*' '\n' \
	  | $(SED) -e '/----/ $(STREKREMSED)' \
	  | $(SED) -e N -e 's/\n/ ----/' \
	  | sort  '-t/' -u +0f -1 +0 \
	  | $(SED) -e 's/.*----//' \
	  | $(SED) -e 's/\(et\/.*T.*\)V/\1/' \
		-e 's/\(e\/.*T.*\)W/\1/' \
		-e 's/\(er\/.*I.*\)V/\1/' \
		-e 's/\(e\/.*B.*\)W/\1/' \
		-e 's/\([^ei]um\/.*B.*\)I/\1/' \
	  | $(SED) -e N -e 's/^\(\([${CH}=]\)*\([^e][^r]\|[e][^r]\|[r][^e]\)\)\/\([A-Zt-z]*\)\n\1e\/\([A-Zt-z]*\)R\([A-Zt-z]*\)$$/\1\/\4\*\1e\/\5\6/g' \
		 -e '$$ p' -e '$$ d' -e P -e D \
	  | tr '*' '\n' \
	  | $(SED) -e N -e 's/^\(\([${CH}=]\)*\)\(\/[AB]*\)E\(.*\)\n\1er\/AI/\1\3\4\*\1er\/AI/' \
		-e '$$ p' -e '$$ d' -e P -e D \
	  | tr '*' '\n' \
	  | $(SED) -e '$(STREKSUBSTSED)' \
		-e 's/\/\([${SUFF}]*\)\([${PRE}]*\)/\/\2\1/' \
		-e 's/\(\([${CH}=]\)*\)\/\([${PRE}]*\)\([${SUFF}]\+\)$$/\1\/\3\*\1\/\3\4/' \
		-e 's/^\([${CH}=]*\)$$/\1\/ /' \
	  | tr '*' '\n' \
	  | $(SED) -e ':START' \
		-e 's/^\([${CH}=]\+\)\/\([${PRE}]*\)\([${SUFF}]\+\)\([${SUFF}]\)/\1\/\2\3\*\1\/\2\4/' \
		-e 't START' \
		-e 's/^\([${CH}=]\+\)\/\([${PRE}]\+\)\(\*\|$$\)/\1\/\*\1\/\2\3/'\
	  |  tr '*' '\n' > munch2.tmp
# This pipe produce a file containing the a line number of munch2.tmp and
# the frequency indicator for that line.  Note that the summation rule
# is not the usual one.
	cat munch2.tmp \
	  | tr -d ' ' \
	  | $(ISPELL) -e -d ./nb.munch.hash \
	  | $(SED) -e 's/^[${CH}=]\+ //' -e '$(STREKSUBSTSED)' \
	  | $(AWK) --source '{i=0; while (i<NF) {i=i+1;print $$i,NR}}' \
	  | sort \
	  | join - ${LANGUAGE}.words \
	  | $(SED) -e 's/\* //' \
	  | cut -d ' ' -f2,3 \
	  | sort -n \
	  | $(SED) -e '$$ p' -e '$$ D' -e ':START' -e '$$ ! N' \
		-e 's/^\([0-9]\+\)\([0-9 ]\+\)\n\1\( [0-9]\+\)$$/\1\2\3/' \
		-e 't START' -e P -e D \
	  | $(AWK) --source '\
		{i = 1;\
		s = 0;\
		{while (i<NF)\
		{i=i+1;\
		if ($$i<5) {s=s+$$i} else {s = s + exp(exp(($$i+9)/15)-1)}}};\
		if (s<=5) {t=s} else {t=-9+15*log(1+log(s))};\
		print $$1, int(t)}' \
	  > munch3.tmp
# This pipe produce the file containing the munched list of words,
# where the rare words we don't want are removed.  What we don't want
# depends on the category of words, and is defined at the start of
# this Makefile.
	cat -n munch2.tmp \
	  | join - munch3.tmp \
	  | cut -d ' ' -f2,3 \
	  | $(AWK) --re-interval --source ${$(subst munched.,CHOOSEFLAG,$@)} \
	  | uniq \
	  | tr -d ' ' \
	  | $(SED) -e '$$ p' -e '$$ D' -e ':START' -e '$$ ! N' \
		-e 's/^\(\([${CH}=]\)\+\)\/\([0-9]*\)\n\1\/\([${SUFF}${PRE}0-9]*\)$$/\1\/\3\4/' \
		-e 's/^\(\([${CH}=]\)\+\/\)\([0-9]*\)\([${PRE}]*\)\([${SUFF}0-9]*\)\n\1\4\([${SUFF}0-9]\+\)$$/\1\3\4\5\6/' \
		-e 't START' -e P -e D \
	  | $(SED) -e 's/\/\([${SUFF}0-9${PRE}]*\)/\/\1\* \1/' \
	  | tr '*' '\n' \
	  | $(SED) -e '/ .*/ s/[^0-9 ]\+/ /g' \
	  | $(SED) -e N -e 's/\n//' \
	  | $(AWK) --source '\
		{i = 1;\
		s = 0;\
		{while (i<NF)\
		{i++;\
		if ($$i<5) {s=s+$$i} else {s = s + exp(exp(($$i+9)/15)-1)}}};\
		if (s<=5) {t=s} else {t=-9+15*log(1+log(s))};\
		print $$1, int(t)}' \
	  | $(AWK) --re-interval --source ${$(subst munched.,CHOOSEROOT,$@)} \
	  | uniq \
	  > $@
#	Comment out the next line if you are debugging.
	rm munch[123].tmp


nb.mch: forkort-nb.txt $(patsubst %,munched.%,${CATHEGORIES}) nb.aff
# Here we make the dictionary that is read by the ispell's buildhash
# program.  The main difficulty is to delete compound words with
# frequency indicator less than COMPOUNDLIMIT accepted in controlled
# compoundwords mode.

# First make a list of words with some compound flag, and a hash-file.
	cat forkort-nb.txt $(patsubst %,munched.%,${CATHEGORIES}) \
	  | tr -d '\=0-9 ' \
	  | $(GREP) "\/.*[z\\_\`]" \
	  > comp1.tmp
	$(BUILDHASH) comp1.tmp nb.aff comp.hash

# Make a list of candidates to be removed.  Exclude all words with
# compound flags and those with frequency indicator bigger than
# COMPOUNDLIMIT.  This could be improved.  One could insist that the
# words forming a word that should be deleted are separated by a
# hyphen at the correct point.  That would complicate things.

	cat -n forkort-nb.txt $(patsubst %,munched.%,${CATHEGORIES}) \
	  | $(GREP) -v "\/.*[z\\_\`]" \
	  | $(AWK) --source '/=/ {if ($$3<${COMPOUNDLIMIT}) {print $$1,$$2,$$3}}' \
	  > comp2.tmp
# Test which words are accepted by ispell.  Output is a list of line
# numbers indicating the lines that can be removed from the munched
# file.
	cat comp2.tmp \
	  | tr -d '\=0-9 ' \
	  | $(ISPELL) -e -d ./comp.hash \
	  | $(SED) -e 's/$$/ xyxyxyxy/' \
	  | $(ISPELL) -l -d ./comp.hash \
	  | $(SED) -e 's/xyxyxyxy/�/' \
	  | tr '\n�' ' \n' \
	  | paste comp2.tmp - \
	  | $(GREP) '	 $$' \
	  | $(SED) -e 's/ .*//' \
	  > comp3.tmp
	@echo Removing `cat comp3.tmp | wc -l` compound root words
# Remove all the line numbers that is found twice, and all words
# containing xxxx and yyyy.  Those words didn't fit in in the munching,
# and since it is few words I don't want to fiddle with them.
	cat -n forkort-nb.txt $(patsubst %,munched.%,${CATHEGORIES}) \
	  | sort -n -m -s +0 -1  comp3.tmp - \
	  | $(SED) -e '/^[0-9]\+$$/,/.*/ D' -e '/\(xxxx\|yyyy\)\// D' \
	  | tr -d '\= 	0-9' \
	  | LC_COLLATE=C sort > $@
	rm -f comp.hash comp[123].tmp*

# TODO:
# If a rare word lies close to a common word, it might be wise to
# remove it from the dictionary.  One possible way is to use a patched
# version of ispell that tries to find matches for all words, also
# those in the dictionary.  Then find the frequency for those words,
# and produce a closeness index from this.  This shouldn't be too hard
# to implement.  The patch for the required ispell flag (-s) is only
# a few lines.  The trickery to use it is more.




nn.mch: ${LANGUAGE}.words nn.aff.munch forkort-nn.txt
	${CATNOHEADER} \
	  | $(GREP) '\*' \
	  | $(SED) -e 's/ .*//' \
	  | tr -d '=' \
	  | $(GREP) '^[${UCH}]' \
	  | $(MUNCHLIST) -v -l nn.aff.munch \
	  > munch1.tmp
	${CATNOHEADER} \
	  | $(GREP) '\*' \
	  | $(SED) -e 's/ .*//' \
	  | tr -d '=' \
	  | $(GREP) -v '^[${UCH}]' \
	  | $(MUNCHLIST) -v -l nn.aff.munch \
	  >> munch1.tmp
	cat forkort-nn.txt munch1.tmp \
	  | $(SED) -e N -e 's/^\(\([${CH}=]\)*\)er\/\(.*F.*\)\n\1rar\/M$$/\1er\/\3D/' \
		-e '$$ p' -e '$$ d' -e P -e D \
	  | LC_COLLATE=C sort > $@.new && mv $@.new $@
	rm munch1.tmp

words.nb: ${LANGUAGE}.words
# Here is a rule to make a list of the most common Norwegian words.
# Which words to include is defined at the top of this Makefile.  Such
# a file is needed to make the word competition work for Norwegian.
# Stupid spell checkers might also want such a file.
	${CATNOHEADER} \
	  | $(GREP) '[BANDS]$$' \
	  | tr -d '*' \
	  | $(AWK) --re-interval --source ${WORDSFILTER} \
	  | tr -d '\"=' \
	  | $(GREP) -v '\(xxxx\|yyyy\|zyzyzy\)' \
	  | sort -f \
	  > $@.new && mv $@.new $@

words.nn: ${LANGUAGE}.words
# No frequency information availiable yet for nynorsk.  So all we can
# do is poick the words marked with a star.
	${CATNOHEADER} \
	  | $(GREP) '\*' \
	  | $(SED) -e 's/ .*//' \
	  | tr -d '\"=' \
	  | $(GREP) -v '\(xxxx\|yyyy\|zyzyzy\)' \
	  | sort -f \
	  > $@.new && mv $@.new $@

# Here is a target that picks words with given frequency.
words.${LANGUAGE}.%: ${LANGUAGE}.words
	${CATNOHEADER} \
	  | $(GREP) '[BANDS]$$' \
	  | $(GREP)  ' $(patsubst words.${LANGUAGE}.%,%,$@) ' \
	  | $(SED) -e 's/ .*//' \
	  | tr -d = \
	  | $(GREP) -v '\(xxxx\|yyyy\|zyzyzy\)' \
	  | sort -f \
	  > $@.new && mv $@.new $@


##########################################################################
# aspell
##########################################################################
aspell: nb.rws nn.rws

nb.dat: Makefile
	rm -f $@
	echo "name    nb"   > $@
	echo "charset iso8859-1"         >> $@
	echo "run-together true"         >> $@
	#echo "special ' =*= = =*= ."     >> $@ # unchanged
	#echo "soundslike none"           >> $@ # unchanged
	echo "special ' =** = =** . =**" >> $@ # like in the Danish file
	echo "soundslike nb"             >> $@ # like in the Danish file
nn.dat: Makefile
	rm -f $@
	echo "name    nn"  > $@
	echo "charset iso8859-1"         >> $@
	echo "run-together true"         >> $@
	#echo "special ' =*= = =*="       >> $@ # unchanged
	#echo "soundslike none"           >> $@ # unchanged
	echo "special ' =** = =** . =**" >> $@ # like in the Danish file
	echo "soundslike nn"             >> $@ # like in the Danish file

nb.rws: nb.mch nb.dat
	rm -f nb_phonet.dat ; ln -s aspell-phonet.dat nb_phonet.dat
	$(ISPELL) -d ./nb -e < nb.mch \
	  | $(PERL) -lane 'for (@F) {s/"(.)/$$1$$1=/g; print; s/($$1)$$1=/$$1/g; print }' \
	  | $(ASPELL) --local-data-dir=`pwd` --lang=nb create master ./nb.rws
	rm nb_phonet.dat
nn.rws: nn.mch nn.dat
	rm -f nn_phonet.dat; ln -s aspell-phonet.dat nn_phonet.dat
	$(ISPELL) -d ./nn -e < nn.mch \
	  | $(PERL) -lane 'for (@F) {s/"(.)/$$1$$1=/g; print; s/($$1)$$1=/$$1/g; print }' \
	  | $(ASPELL) --local-data-dir=`pwd` --lang=nn create master ./nn.rws
	rm nn_phonet.dat

install-aspell: install-aspell-nb install-aspell-nn
nb.multi:
	echo add nb.rws > $@
nn.multi:
	echo add nn.rws > $@
install-aspell-nb: nb.rws nb.dat nb.multi
	$(INSTALL) -d $(DESTDIR)$(aspelldir)/
	$(INSTALL) -d $(DESTDIR)$(aspellsharedir)/
	$(INSTALL_DATA) nb.multi $(DESTDIR)$(aspelldir)/nb.multi

	$(INSTALL_DATA) nb.dat $(DESTDIR)$(aspellsharedir)/nb.dat
	$(INSTALL_DATA) nb.rws $(DESTDIR)$(aspelldir)/nb.rws
	echo "add no.multi" > $(DESTDIR)$(aspelldir)/norwegian.alias
	$(INSTALL_DATA) aspell-phonet.dat $(DESTDIR)$(aspellsharedir)/nb_phonet.dat

install-aspell-nn: nn.rws nn.dat nn.multi
	$(INSTALL) -d $(DESTDIR)$(aspelldir)/
	$(INSTALL) -d $(DESTDIR)$(aspellsharedir)/
	$(INSTALL_DATA) nn.multi $(DESTDIR)$(aspelldir)/nn.multi

	$(INSTALL_DATA) nn.dat $(DESTDIR)$(aspellsharedir)/nn.dat
	$(INSTALL_DATA) nn.rws $(DESTDIR)$(aspelldir)/nn.rws
	$(INSTALL_DATA) aspell-phonet.dat $(DESTDIR)$(aspellsharedir)/nn_phonet.dat

##########################################################################
# Myspell (OpenOffice.org)
##########################################################################

# Generate list of all nb and nn words to be used when generating the
# munched lists.
nb.munch: ${LANGUAGE}.words Makefile
	${CATNOHEADER} \
	  | $(GREP) -e '[A-Z]$$' \
	  | $(SED) -e 's/ .*//' -e '$(ALPHASUBSTSED)' -e 's/ \*//' \
	  | $(GREP) '^[${CH}]' > $@.new && mv $@.new $@
nn.munch: ${LANGUAGE}.words Makefile
	${CATNOHEADER} \
	  | $(GREP) '\*' \
	  | $(SED) -e 's/ .*//' -e '$(ALPHASUBSTSED)' -e 's/ \*//' \
	  | $(GREP) '^[${CH}]' > $@.new && mv $@.new $@

myspell: nb_NO.mydict nb_NO.myaff nn_NO.mydict nn_NO.myaff $(TH_NB2).idx $(TH_NN2).idx
nb_NO.myaff.munch: nb_NO.myheader nb.aff.munch
	$(ISPELLAFF2MYSPELL) --charset=latin1 --split=200 \
		--myheader=nb_NO.myheader nb.aff.munch > nb_NO.myaff.munch
nn_NO.myaff.munch: nn_NO.myheader nn.aff.munch
	$(ISPELLAFF2MYSPELL) --charset=latin1 --split=200 \
		--myheader=nn_NO.myheader nn.aff.munch > nn_NO.myaff.munch
nb_NO.myaff: nb_NO.myheader nb.aff
	$(ISPELLAFF2MYSPELL) --charset=latin1 --split=200 \
		--myheader=nb_NO.myheader nb.aff > nb_NO.myaff
nn_NO.myaff: nn_NO.myheader nn.aff
	$(ISPELLAFF2MYSPELL) --charset=latin1 --split=200 \
		--myheader=nn_NO.myheader nn.aff > nn_NO.myaff
nb.munched: nb.munch nb_NO.myaff.munch
	munch nb.munch nb_NO.myaff.munch > $@
nn.munched: nn.munch nn_NO.myaff.munch
	munch nn.munch nn_NO.myaff.munch > $@
slow.nb_NO.mydict: nb.munched
	cat forkort-nb.txt nb.munched \
	  | $(SED) -e '$(STREKREMSED)' -e 's/"//g' \
	  | LC_COLLATE=C ./scripts/sortflags | LC_COLLATE=C sort > $@.new && \
	  mv $@.new $@
slow.nn_NO.mydict: nn.munched
	cat forkort-nn.txt nn.munched \
	  | $(SED) -e '$(STREKREMSED)' -e 's/"//g' \
	  | LC_COLLATE=C ./scripts/sortflags | LC_COLLATE=C sort > $@.new && \
	  mv $@.new $@
nb_NO.mydict: nb.mch
	( wc -l nb.mch|$(AWK) '{print $$1}'; cat nb.mch ) | $(SED) 's/"//g' > $@
nn_NO.mydict: nn.mch
	( wc -l nn.mch|$(AWK) '{print $$1}'; cat nn.mch ) | $(SED) 's/"//g' > $@

# Build OpenOffice (myspell) thesaurus files from the word list.  Need the
# thescoder program from
# <URL:https://sourceforge.net/project/showfiles.php?group_id=128318>
# These rules are for OpenOffice 1.x, and do not work for OpenOffice.org 2.x.
$(TH_NB1).idx:  thesaurus-nb.txt
	thescoder thesaurus-nb.txt $(TH_NB1)
$(TH_NN1).idx: thesaurus-nn.txt
	thescoder thesaurus-nn.txt $(TH_NN1)
$(TH_NB2).idx:	thesaurus-nb.txt scripts/thes_to_dat
	scripts/thes_to_dat < thesaurus-nb.txt > $(TH_NB2).dat
	# Using th_gen_idx.pl from libmythes-dev
	/usr/share/mythes/th_gen_idx.pl -o $(TH_NB2).idx < $(TH_NB2).dat
$(TH_NN2).idx:	thesaurus-nn.txt scripts/thes_to_dat
	scripts/thes_to_dat < thesaurus-nn.txt > $(TH_NN2).dat
	# Using th_gen_idx.pl from libmythes-dev
	/usr/share/mythes/th_gen_idx.pl -o $(TH_NN2).idx < $(TH_NN2).dat

install-myspell: install-myspell-nb install-myspell-thes1-nb \
		 install-myspell-hyph-nb \
		 install-myspell-nn install-myspell-thes1-nn \
		 install-myspell-hyph-nn
install-myspell-nb: nb_NO.mydict nb_NO.myaff
	$(INSTALL) -d $(DESTDIR)$(myspelldir)/
	$(INSTALL_DATA) nb_NO.mydict $(DESTDIR)$(myspelldir)/nb_NO.dic
	$(INSTALL_DATA) nb_NO.myaff $(DESTDIR)$(myspelldir)/nb_NO.aff

install-myspell-thes1-nb: $(TH_NB1).idx
	$(INSTALL) -d $(DESTDIR)$(thesdir)/
	$(INSTALL_DATA) $(TH_NB1).dat $(DESTDIR)$(thesdir)/$(TH_NB1).dat
	$(INSTALL_DATA) $(TH_NB1).idx $(DESTDIR)$(thesdir)/$(TH_NB1).idx
	# Need to be done after the package installation
	@echo "remember to add $(TH_NB1_DICT) to $(DESTDIR)$(thesdir)/dictionary.lst"

install-myspell-thes1-nn: $(TH_NN1).idx
	$(INSTALL) -d $(DESTDIR)$(thesdir)/
	$(INSTALL_DATA) $(TH_NN1).dat $(DESTDIR)$(thesdir)/$(TH_NN1).dat
	$(INSTALL_DATA) $(TH_NN1).idx $(DESTDIR)$(thesdir)/$(TH_NN1).idx
	# Need to be done after the package installation
	@echo "remember to add $(TH_NN1_DICT) to $(DESTDIR)$(thesdir)/dictionary.lst"

install-myspell-thes2-nb: $(TH_NB2).idx
	$(INSTALL) -d $(DESTDIR)$(thesdir)/
	$(INSTALL_DATA) $(TH_NB2).dat $(DESTDIR)$(thesdir)/$(TH_NB2).dat
	$(INSTALL_DATA) $(TH_NB2).idx $(DESTDIR)$(thesdir)/$(TH_NB2).idx
install-myspell-thes2-nn: $(TH_NN2).idx
	$(INSTALL) -d $(DESTDIR)$(thesdir)/
	$(INSTALL_DATA) $(TH_NN2).dat $(DESTDIR)$(thesdir)/$(TH_NN2).dat
	$(INSTALL_DATA) $(TH_NN2).idx $(DESTDIR)$(thesdir)/$(TH_NN2).idx

# Remove hyphenation{} block, as it is unsupported by OpenOffice.org as far as I
# know.  Convert and document charset.
hyph_no_NO.dic: nohyphb.tex Makefile
	( \
	  echo ISO8859-1 ; \
	  $(SED) \
		-e '/^\\hyphenation{/ , /^}/ D' \
		-e 's/\^\^e5/�/g' \
		-e 's/\^\^f8/�/g' \
		-e 's/\^\^e6/�/g' \
		-e 's/\^\^e9/�/g' \
		-e 's/\^\^e8/�/g' \
		-e 's/\^\^ea/�/g' \
		-e 's/\^\^f2/�/g' \
		-e 's/\^\^f3/�/g' \
		-e 's/\^\^f4/�/g' \
		-e 's/\^\^fc/�/g' < nohyphb.tex \
	  | $(EGREP) -v '^%|^\\|\}' \
	) > $@

install-myspell-hyph-nb:  hyph_no_NO.dic
	$(INSTALL) -d $(DESTDIR)$(hyphendir)/
	$(INSTALL_DATA) hyph_no_NO.dic $(DESTDIR)$(hyphendir)/hyph_nb_NO.dic
	@echo "remember to add 'HYPH nb NO hyph_nb_NO' to $(DESTDIR)$(hyphendir)/dictionary.lst"

install-myspell-nn: nn_NO.mydict nn_NO.myaff # $(TH_NN1).idx
	$(INSTALL) -d $(DESTDIR)$(myspelldir)/
	$(INSTALL_DATA) nn_NO.mydict $(DESTDIR)$(myspelldir)/nn_NO.dic
	$(INSTALL_DATA) nn_NO.myaff $(DESTDIR)$(myspelldir)/nn_NO.aff

install-myspell-hyph-nn: hyph_no_NO.dic
	$(INSTALL) -d $(DESTDIR)$(hyphendir)/
	$(INSTALL_DATA) hyph_no_NO.dic $(DESTDIR)$(hyphendir)/hyph_nn_NO.dic
	@echo "remember to add 'HYPH nn NO nn_NO' to $(DESTDIR)$(hyphendir)/dictionary.lst"

# Check which words in missing.<lang> are also in the word list, and
# thus can be removed from missing.<lang>.
check-missing: words.nb words.nn
	for l in nb nn; do \
		echo "Words listed in missing.$$l also listed in words.$$l:" ;\
		(for w in `$(GREP) -v '#' missing.$$l`; do \
			$(GREP) "^$$w\$$" words.$$l; \
		done) | $(SED) 's/^/  /'; \
	done

#
# Update frequency information based on the available information.
#
ordlistf.zip:
	rm -f ordlistf.zip
	wget http://helmer.aksis.uib.no/nta/ordlistf.zip
ORDLISTF.TXT: ordlistf.zip
	rm -f ORDLISTF.TXT
	unzip -o ordlistf.zip
	fromdos ORDLISTF.TXT
freq-update: ORDLISTF.TXT
	scripts/freq-update ORDLISTF.TXT ${LANGUAGE}.words > ${LANGUAGE}.words.updated
	mv ${LANGUAGE}.words.updated ${LANGUAGE}.words

#
NBSPELINGSTATUS = http://tyge.sslug.dk/~korsvoll/nb.speling.org/htdocs/status
NNSPELINGSTATUS = http://tyge.sslug.dk/~korsvoll/nn.speling.org/htdocs/status

web-update: source-nb
source-nb.gz:
	wget $(NBSPELINGSTATUS)/source.gz -O $@.new && mv $@.new $@
source-nn.gz:
	wget $(NNSPELINGSTATUS)/source.gz -O $@.new && mv $@.new $@
source-nb: source-nb.gz Makefile
	gunzip < source-nb.gz > $@.new && mv $@.new $@
source-nn: source-nn.gz Makefile
	gunzip < source-nn.gz > $@.new && mv $@.new $@

# Update thesaurus files from no.speling.org.  These rules are now obsolete

thesaurus-nb.new: source-nb scripts/speling-extract-synonyms Makefile
	echo '$(theschars)' > thesaurus-nb.new
	scripts/speling-extract-synonyms < source-nb >> thesaurus-nb.new

	@echo "============================================================="
	@echo "warning: Obsolete rule $@.  Use thes-nb-update instead"
	@echo "============================================================="
thesaurus-nn.new: source-nn scripts/speling-extract-synonyms Makefile
	echo '$(theschars)' > thesaurus-nn.new
	scripts/speling-extract-synonyms < source-nn >> thesaurus-nn.new
# Not obsolete yet.  merg.net do not have nn lists
#	@echo "============================================================="
#	@echo "warning: Obsolete rule $@.  Use thes-nb-update instead"
#	@echo "============================================================="

##########################################################################
# OpenOffice.org distribution file (zip-files)
##########################################################################

ooo-dist: no_NO-pack2.zip # no_NO-pack1.zip 
no_NO-pack: myspell
	rm -rf ooo-dist
	mkdir ooo-dist

	$(MAKE) ooo-dist/nb_NO.zip ooo-dist/nn_NO.zip
	( \
	  echo "nb,NO,nb_NO,Norwegian Bokm�l (Norway),nb_NO.zip" ; \
	  echo "nn,NO,nn_NO,Norwegian Nynorsk (Norway),nn_NO.zip" ; \
	) > ooo-dist/spell.txt

	$(MAKE) ooo-dist/hyph_nb_NO.zip ooo-dist/hyph_nn_NO.zip
	( \
	  echo "nb,NO,hyph_nb_NO,Norwegian Bokm�l (Norway),hyph_nb_NO.zip" ; \
	  echo "nn,NO,hyph_nn_NO,Norwegian Nynorsk (Norway),hyph_nn_NO.zip" ; \
	) > ooo-dist/hyph.txt

	$(INSTALL_DATA) README NEWS ooo-dist/.

# Pack for OpenOffice.org v1, with thesaurus using the old format
no_NO-pack1.zip: no_NO-pack
	$(MAKE) ooo-dist/$(TH_NB1).zip ooo-dist/$(TH_NN1).zip
	( \
	  echo "nb,NO,$(TH_NB1),Norwegian Bokm�l (Norway),$(TH_NB1).zip" ; \
	  echo "nn,NO,$(TH_NN1),Norwegian Nynorsk (Norway),$(TH_NN1).zip" ; \
	) > ooo-dist/thes1.txt

	rm -f ../no_NO-pack-$(VERSION).zip
	(cd ooo-dist; zip ../no_NO-pack1-$(VERSION).zip \
	    README NEWS \
	    spell.txt nb_NO.zip nn_NO.zip \
	    thes1.txt $(TH_NB1).zip $(TH_NN1).zip \
	    hyph.txt hyph_nb_NO.zip  hyph_nn_NO.zip)

# Pack for OpenOffice.org v2, with thesaurus using the new format
no_NO-pack2.zip: no_NO-pack
	$(MAKE) ooo-dist/$(TH_NB2).zip ooo-dist/$(TH_NN2).zip
	( \
	  echo "nb,NO,$(TH_NB2),Norwegian Bokm�l (Norway),$(TH_NB2).zip" ; \
	  echo "nn,NO,$(TH_NN2),Norwegian Nynorsk (Norway),$(TH_NN2).zip" ; \
	) > ooo-dist/thes2.txt

	rm -f ../no_NO-pack-$(VERSION).zip
	(cd ooo-dist; zip ../no_NO-pack2-$(VERSION).zip \
	    README NEWS \
	    spell.txt nb_NO.zip nn_NO.zip \
	    thes2.txt $(TH_NB2).zip $(TH_NN2).zip \
	    hyph.txt hyph_nb_NO.zip  hyph_nn_NO.zip)


ooo-dist/nb_NO.zip:
	$(MAKE) install-myspell-nb DESTDIR= myspelldir=ooo-dist/nb_NO
	( \
	  echo Myspell dictionary ; \
	  echo ------------------ ; \
	  echo ; \
	  echo "Language: Norwegian Bokm�l (nb NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo "DICT nb NO nb_NO" ; \
	) > ooo-dist/nb_NO/README_nb_NO.txt
	(cd ooo-dist/nb_NO; zip ../nb_NO.zip *)

ooo-dist/$(TH_NB1).zip: $(TH_NB1).idx
	$(MAKE) install-myspell-thes1-nb DESTDIR= myspelldir=ooo-dist/$(TH_NB1)
	( \
	  echo Myspell thesaurus for OpenOffice.org v1 ; \
	  echo ----------------- ; \
	  echo ; \
	  echo "Language: Norwegian Bokm�l (nb NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo $(TH_NB1_DICT) ; \
	) > ooo-dist/$(TH_NB1)/README_$(TH_NB1).txt
	(cd ooo-dist/$(TH_NB1); zip ../$(TH_NB1).zip *)

ooo-dist/$(TH_NB2).zip: $(TH_NB2).idx
	$(MAKE) install-myspell-thes2-nb DESTDIR= myspelldir=ooo-dist/$(TH_NB2)
	( \
	  echo Myspell thesaurus for OpenOffice.org v2 ; \
	  echo ----------------- ; \
	  echo ; \
	  echo "Language: Norwegian Bokm�l (nb NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo $(TH_NB2_DICT) ; \
	) > ooo-dist/$(TH_NB2)/README_$(TH_NB2).txt
	(cd ooo-dist/$(TH_NB2); zip ../$(TH_NB2).zip *)

ooo-dist/hyph_nb_NO.zip:
	$(MAKE) install-myspell-hyph-nb DESTDIR= myspelldir=ooo-dist/hyph_nb_NO
	( \
	  echo Myspell hyphenation ; \
	  echo ------------------- ; \
	  echo ; \
	  echo "Language: Norwegian Bokm�l (nb NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo "HYPH nb NO nb_NO" ; \
	) > ooo-dist/hyph_nb_NO/README_hyph_nb_NO.txt
	(cd ooo-dist/hyph_nb_NO; zip ../hyph_nb_NO.zip *)

ooo-dist/nn_NO.zip:
	$(MAKE) install-myspell-nn DESTDIR= myspelldir=ooo-dist/nn_NO
	( \
	  echo Myspell dictionary ; \
	  echo ------------------ ; \
	  echo ; \
	  echo "Language: Norwegian Nynorsk (nn NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo DICT nn NO nn_NO ; \
	) > ooo-dist/nn_NO/README_nn_NO.txt
	(cd ooo-dist/nn_NO; zip ../nn_NO.zip *)

ooo-dist/$(TH_NN1).zip: $(TH_NB1).idx
	$(MAKE) install-myspell-thes1-nn DESTDIR= myspelldir=ooo-dist/$(TH_NN1)
	( \
	  echo Myspell thesaurus for OpenOffice.org v1 ; \
	  echo ----------------- ; \
	  echo ; \
	  echo "Language: Norwegian Nynorsk (nn NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo $(TH_NN1_DICT) ; \
	) > ooo-dist/$(TH_NN1)/README_$(TH_NN1).txt
	(cd ooo-dist/$(TH_NN1); zip ../$(TH_NN1).zip *)

ooo-dist/$(TH_NN2).zip: $(TH_NB2).idx
	$(MAKE) install-myspell-thes2-nn DESTDIR= myspelldir=ooo-dist/$(TH_NN2)
	( \
	  echo Myspell thesaurus for OpenOffice.org v2 ; \
	  echo ----------------- ; \
	  echo ; \
	  echo "Language: Norwegian nynorsk (nn NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo $(TH_NN2_DICT) ; \
	) > ooo-dist/$(TH_NN2)/README_$(TH_NN2).txt
	(cd ooo-dist/$(TH_NN2); zip ../$(TH_NN2).zip *)

ooo-dist/hyph_nn_NO.zip:
	$(MAKE) install-myspell-hyph-nn DESTDIR= myspelldir=ooo-dist/hyph_nn_NO
	( \
	  echo Myspell hyphenation ; \
	  echo ------------------- ; \
	  echo ; \
	  echo "Language: Norwegian Nynorsk (nn NO)" ; \
	  echo "Origin:   Generated from the spell-norwegian source v$(VERSION)" ; \
	  echo "License:  GNU General Public license" ; \
	  echo "Author:   The spell-norwegian project, <URL:https://alioth.debian.org/projects/spell-norwegian/>" ; \
	  echo ; \
	  echo "HYPH nn NO nn_NO" ; \
	) > ooo-dist/hyph_nn_NO/README_hyph_nn_NO.txt
	(cd ooo-dist/hyph_nn_NO; zip ../hyph_nn_NO.zip *)

##########################################################################
# Compare to speling.org database
##########################################################################

speling-good.nb:
	GET $(NBSPELINGSTATUS)/words.good.gz     | gunzip|sort > $@
speling-new.nb: words.nb
	GET $(NBSPELINGSTATUS)/words.disputed.gz | gunzip|sort > speling-disputed.nb
	sort words.nb      > tmp-nb
	sort speling-good.nb speling-disputed.nb > tmp-nb-new-full
	comm -13 tmp-nb  speling-good.nb > $@
	comm -23 tmp-nb  tmp-nb-new-full > speling-missing.nb
	@echo -n "nye p� speling.no:     "; wc -l $@
	@echo -n "mangler p� speling.no: "; wc -l speling-missing.nb
	@echo -n "omstridt p� speling.no: "; wc -l speling-disputed.nb
	rm tmp-nb  tmp-nb-new-full

speling-good.nn:
	GET $(NNSPELINGSTATUS)/words.good.gz     | gunzip|sort > $@
speling-new.nn: words.nn
	GET $(NNSPELINGSTATUS)/words.disputed.gz | gunzip|sort > speling-disputed.nn
	sort words.nn      > tmp-nn
	sort speling-good.nn speling-disputed.nn > tmp-nn-new-full
	comm -13 tmp-nn  speling-good.nn > $@
	comm -23 tmp-nn  tmp-nn-new-full > speling-missing.nn
	@echo -n "nye p� speling.no:     "; wc -l $@
	@echo -n "mangler p� speling.no: "; wc -l speling-missing.nn
	@echo -n "omstridt p� speling.no: "; wc -l speling-disputed.nn
	rm tmp-nn tmp-nn-new-full

update-from-spelingorg:
	$(RM) source-nb* speling-good.nb source-nn* speling-good.nn
	$(MAKE) spelingorg2norskwords

spelingorg2norskwords: norsk.words source-nb speling-good.nb source-nn speling-good.nn
	scripts/speling2words norsk.words \
		source-nb speling-good.nb \
		source-nn speling-good.nn > $@.tmp && mv $@.tmp norsk.words

##########################################################################
# Compare to synonymer.merg.net database
##########################################################################

THES_URL = http://synonymer.merg.net/download/thesaurus.txt

thesaurus-nb-mergnet.txt:
	wget $(THES_URL) -O - > thesaurus-nb-mergnet.txt.new && \
	  mv thesaurus-nb-mergnet.txt.new thesaurus-nb-mergnet.txt

synonymer-new.nb: words.nb thesaurus-nb-mergnet.txt
	$(GREP) -v '#' thesaurus-nb-mergnet.txt | tr ";/ " "\n\n\n"| \
		sort -u > thesaurus-words.txt
	sort words.nb > words.nb.sorted
	comm -13 words.nb.sorted thesaurus-words.txt > $@
	wc -l $@
#	rm words.nb.sorted thesaurus-words.txt

thesaurus-nb.txt-update: thesaurus-nb-mergnet.txt
	( \
	echo '$(theschars)' && \
	$(GREP) -v '#' thesaurus-nb-mergnet.txt | sed 's/;/; /g' \
	) > thesaurus-nb.txt.new && \
	mv thesaurus-nb.txt.new thesaurus-nb.txt

##########################################################################
# Make tarball
##########################################################################

dist: $(distdir).tar.gz ooo-dist
$(distdir).tar.gz: distdir
	-chmod -R a+r $(distdir)
	GZIP=$(GZIP_ENV) $(TAR) chozf $(distdir).tar.gz $(distdir)
	-rm -rf $(distdir)

distcheck: dist
	tar zxf $(distdir).tar.gz && \
		$(MAKE) -C $(distdir) && \
		$(MAKE) -C $(distdir) DESTDIR=destdir install && \
		rm -rf $(distdir)

distdir: $(DISTFILES)
	-rm -rf $(distdir)
	mkdir $(distdir)
	-chmod 777 $(distdir)
	for file in $(DISTFILES); do \
	  d=$(srcdir); \
	  if test -d $$d/$$file; then \
	    cp -pr $$d/$$file $(distdir)/$$file; \
	  else \
	    test -f $(distdir)/$$file \
	    || ln $$d/$$file $(distdir)/$$file 2> /dev/null \
	    || cp -p $$d/$$file $(distdir)/$$file || :; \
	  fi; \
	done
	for subdir in $(SUBDIRS); do \
	  if test "$$subdir" = .; then :; else \
	    test -d $(distdir)/$$subdir \
	    || mkdir $(distdir)/$$subdir \
	    || exit 1; \
	    chmod 777 $(distdir)/$$subdir; \
	    (cd $$subdir && $(MAKE) $(AM_MAKEFLAGS) top_distdir=../$(distdir) distdir=../$(distdir)/$$subdir distdir) \
	      || exit 1; \
	  fi; \
	done

current-stats.txt:
	./speling-stats | iconv -f latin1 -t utf-8 > current-stats.txt

##########################################################################
# Clean up generated files
##########################################################################

clean:
	rm -f core *.hash *.stat *.cnt munch[123].tmp \
	      nb.mch nn.mch \
	      nb.aff nn.aff \
	      nb.aff.munch nn.aff.munch \
	      words.nb words.nn \
	      nb_NO.myaff nn_NO.myaff \
	      nb_NO.mydict nn_NO.mydict \
	      $(TH_NB1).dat $(TH_NB1).idx \
	      $(TH_NN1).dat $(TH_NN1).idx \
	      $(TH_NB2).dat $(TH_NB2).idx \
	      $(TH_NN2).dat $(TH_NN2).idx \
	      nb.multi nn.multi \
	      nb.dat nn.dat \
	      nb.rws nn.rws \
	      comp[123].tmp* munched.*
	rm -f nb.munch nn.munch nb_NO.myaff.munch nn_NO.myaff.munch
	rm -f nb.munched nn.munched nb.multi nn.multi
	rm -f hyph_no_NO.dic
	rm -f .#* *~
distclean: clean
	rm -f ORDLISTF.TXT ordlistf.zip
	rm -f source-nb.gz source-nb source-nn.gz source-nn
	rm -f thesaurus-nb.new thesaurus-nn.new
	rm -f $(distdir).tar.gz
	rm -fr ooo-dist no_NO-pack1-$(VERSION).zip no_NO-pack2-$(VERSION).zip


#	The following target is used in the English makefile, and is
#	required to be present in all other language Makefiles as
#	well, even though it doesn't have to do anything in those
#	directories.
#
kitclean:

#
#	The following target is used in the English makefile, and is
#	required to be present in all other language Makefiles as
#	well, even though it doesn't have to do anything in those
#	directories.
#
dictclean:
	rm -f $(patsubst %,munched.%,${CATHEGORIES}) \
	      words.nb words.nn
